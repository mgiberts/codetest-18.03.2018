const path = require('path')
const fs = require('fs')

const getDataFilesNames = (localPath) => {
  try {
    return fs.readdirSync(path.join(__dirname, localPath), 'utf-8')
  } catch (err) {
    throw err
  }
}

const getData = (localPath = '../data') => {
  const data = {questions: []}

  getDataFilesNames(localPath).forEach((file) => {
    if (file.match(/.json$/)) {
      if (file.match(/^[\d]+(,[\d]+)+.json$/)) {
        data.questions = data.questions.concat(require(`${localPath}/${file}`))
      } else {
        data[file.replace(/.json$/, '')] = require(`${localPath}/${file}`)
      }
    }
  })

  return JSON.stringify(data)
}

module.exports = {
  getData
}
