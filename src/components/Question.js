import React from 'react'
import PropTypes from 'prop-types'

const markup = (html) => ({__html: html})

const Question = (
  {question, answer, tocId},
  {expanded, toggle}
) => (
  <div className={'question-container'}>
    <div
      className={'question'}
      dangerouslySetInnerHTML={markup(question)}
      onClick={() => toggle(tocId)}
    />
    {expanded.find(id => id === tocId) && <div
      className={'answer'}
      dangerouslySetInnerHTML={markup(answer)}
    />}
  </div>
)

Question.contextTypes = {
  expandAll: PropTypes.bool,
  expanded: PropTypes.arrayOf(PropTypes.string),
  toggle: PropTypes.func
}

export default Question
