import React from 'react'
import SectionGroup from './SectionsGroup'
import Question from './Question'

export default ({title, nodes, questions}) => (
  <div className={'section'}>
    <div className={'section-title'}>{title}</div>
    <SectionGroup nodes={nodes} />
    {questions && questions.map(question => <Question key={question.qa_id} {...question} />)}
  </div>
)
