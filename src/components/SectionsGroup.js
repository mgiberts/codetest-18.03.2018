import React from 'react'
import Section from './Section'

export default ({nodes}) => (
  <div>
    {nodes.map(section => <Section key={section.id} {...section} />)}
  </div>
)
