import React, { Component} from 'react'
import PropTypes from 'prop-types'
import {getSectionsTree, hydrateSections} from './utils/dataStructures'
import SectionsGroup from './components/SectionsGroup'
import './style.css'

const data = DATA

console.log('data states', data.states)

class App extends Component {
  state = {
    expanded: []
  }

  toggleQuestion = (tocId) => {
    const {expanded} = this.state
    if(expanded.find(id => id === tocId)) {
      expanded.splice(expanded.indexOf(tocId), 1)
    } else {
      expanded.push(tocId)
    }
    this.setState({expanded})
  }

  expandAll = () => {
    this.setState({expanded: data.questions.map(({tocId}) => tocId)})
  }

  collapseAll = () => {
    this.setState({expanded: []})
  }

  setCustomExpanded = (expanded) => {
    this.setState({expanded})
  }

  getChildContext() {
    return {
      expanded: this.state.expanded,
      toggle: this.toggleQuestion
    }
  }

  render() {
    const {expanded} = this.state
    return (
      <div className={'app'}>
        <h1>Q&A</h1>
        <button onClick={this.expandAll}>Expand All</button>
        {data.states.map(({expanded}, key) => (
          <button
            key={key}
            onClick={() => this.setCustomExpanded(expanded)}
          >
            Expand v{key + 1}
          </button>
        ))}
        {expanded.length > 0 && <button onClick={this.collapseAll}>Collapse All ({expanded.length})</button>}
        <SectionsGroup nodes={getSectionsTree(hydrateSections(data.sections, data.questions))[-1].nodes} />
      </div>
    )
  }
}

App.childContextTypes = {
  expanded: PropTypes.arrayOf(PropTypes.string),
  toggle: PropTypes.func
}

export default App
