
export const hydrateSections = (sections, questions = []) => {
  return sections.map(section => ({
    ...section,
    questions: questions.filter(({sectionId}) => sectionId === section.id)}
  ))
}

// Get Child tree from flat array
export const getSectionsTree = (sections) => {
  const root = {id:-2, parentId: null, nodes: []}
  const tree = { '-2': root}

  for (let i = 0; i < sections.length; i++) {
    tree[sections[i].id] = {...sections[i], nodes: []}

    if (!tree[sections[i].parentId]) { // TODO: new root or data error? Will get lost.
      tree[sections[i].parentId] = {id: 10, parentId: null, nodes: []}
    }

    tree[sections[i].parentId].nodes.push(tree[sections[i].id])
  }

  return tree
}
